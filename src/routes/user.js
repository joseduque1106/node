const express = require('express');
const User = require('../models/user');
const bodyParser = require('body-parser');


const app = express();

app.use(bodyParser.json());

app.post('/users/create', (req, res) => {
    let body = req.body;

    let user = new User({
        nombre: body.nombre,
        cedula: body.cedula,
        celular: body.celular,
        email: body.email,
        password: body.password
    });
    user.save((err, userDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        return res.status(200).json({
            ok: true,
            usuario: userDB
        });
    });
});

app.get('/users/list', function(req, res) {

    User.find({})
        .exec((err, userDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false.
                    err
                });
            }
            res.json({
                ok: true,
                userDB
            });
        });
});
app.put('/Usuario/:id', (req, res) => {

    let id = req.params.id;
    let body = req.body;

    Usuario.findByIdAndUpdate(id, body, { new: true }, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: true,
                err
            });
        }

        res.json({
            ok: true,
            usuarioDB
        });

    });
})

app.delete('/Usuario/:id', function(req, res) {
    let id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            usuarioDB
        });
    });
});

module.exports = app;