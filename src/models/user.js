const mongoose = require('mongoose');


const Schema = mongoose.Schema;

let userSchema = new Schema({
    nombre: { type: String, required: [true, 'el nombre es necesario'] },
    cedula: { type: String, required: [true, 'la cedula es necesaria'] },
    celular: { type: String, required: [true, 'el celular es necesario es necesaria'] },
    email: { type: String, unique: true, required: [true, 'el correo es obligatorio'] },
    password: { type: String, required: [true, 'el correo es obligatorio'] }

});


module.exports = mongoose.model('Usuario', userSchema);