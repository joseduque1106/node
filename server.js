const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const app = express();

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json());

//configuracion de las rutas globales
app.use(require('./src/routes/index'));


mongoose.connect('mongodb://localhost:27017/prueba', (err, res) => {
    if (err) throw err;
    console.log("Base de datos online");
});

app.listen(3000, (err, res) => {
    if (err) throw err;
    console.log("escuchando peticiones por el puerto 3000");
});